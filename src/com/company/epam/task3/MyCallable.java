package com.company.epam.task3;

import java.util.concurrent.Callable;

public class MyCallable implements Callable <long[]> {

  FibonacciCallable fibonacciC;
  String name;

  public MyCallable(FibonacciCallable fibonacciC, String name) {
    this.fibonacciC = fibonacciC;
    this.name = name;
  }

  @Override
  public long[] call() throws Exception {
    return fibonacciC.produceF2();
  }
}
