package com.company.epam.task3;

import static java.util.concurrent.TimeUnit.SECONDS;

import com.company.epam.task2.Fibonacci;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

public class View {

  public static void main(String[] args) {

    Fibonacci fibonacci1 = new Fibonacci(60);
    Fibonacci fibonacci2 = new Fibonacci(50);

    FibonacciCallable fibonacciC1 = new FibonacciCallable(60);
    FibonacciCallable fibonacciC2 = new FibonacciCallable(50);

    Runnable r1 = new Runnable() {
      @Override
      public void run() {
        fibonacci1.produceF();
      }
    };

    Runnable r2 = new Runnable() {
      @Override
      public void run() {
        fibonacci2.produceF();
      }
    };
//Executor
//    Executor ex1 = Executors.newFixedThreadPool(2);
//    ex1.execute(r1);
//    ex1.execute(r2);

//    MyExecutor myex1 = new MyExecutor(fibonacci1);
//    MyExecutor myex2 = new MyExecutor(fibonacci2);

//Callable
//    MyCallable mc1 = new MyCallable(fibonacciC1, "first");
//    MyCallable mc2 = new MyCallable(fibonacciC1, "second");
//    ExecutorService es1 = Executors.newFixedThreadPool(2);
//    es1.submit(mc1);
//    es1.submit(mc2);

    ScheduledExecutorService ses = Executors.newScheduledThreadPool(2);
    ses.scheduleAtFixedRate(r1, 5, 10, SECONDS);
    ses.scheduleAtFixedRate(r2, 5, 10, SECONDS);

  }

}
