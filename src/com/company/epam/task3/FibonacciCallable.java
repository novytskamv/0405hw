package com.company.epam.task3;

import com.company.epam.task2.FibonacciThread;
import java.util.concurrent.ExecutorService;

public class FibonacciCallable {

  int n;

  public FibonacciCallable(int n) {
    this.n = n;
  }

  public void produceF(){
    long fib [] = new long[n];
    fib[0] = 0;
    fib[1] = 1;
    for(int i = 2; i < fib.length; i++ ){
      fib[i]=fib[i-1]+fib[i-2];
    }
    for(long s: fib){
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println(FibonacciThread.currentThread().getName()+" "+s);
    }
  }

  public long[] produceF2(){
    long fib [] = new long[n];
    fib[0] = 0;
    fib[1] = 1;
    for(int i = 2; i < fib.length; i++ ){
      fib[i]=fib[i-1]+fib[i-2];
    }
    try {
      for(long l: fib){
        Thread.sleep(100);
        System.out.println(l+" "+Thread.currentThread().getName());
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return fib;
  }

}
