package com.company.epam.task3;

import com.company.epam.task2.Fibonacci;
import java.util.concurrent.Executor;

public class MyExecutor implements Executor {

  Fibonacci fibonacci;

  Runnable runnable = new Runnable() {
    @Override
    public void run() {
      fibonacci.produceF();
    }
  };

  public MyExecutor(Fibonacci fibonacci) {
    this.fibonacci = fibonacci;
    this.execute(runnable);
   }


  @Override
  public void execute(Runnable runnable) {
    runnable.run();
  }
}
