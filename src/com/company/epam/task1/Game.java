package com.company.epam.task1;

public class Game {

  public static void main(String[] args) {

    Ping_Pong ping_pong = new Ping_Pong();

    PingPongThread th1 = new PingPongThread(ping_pong, "ping");
    PingPongThread th2 = new PingPongThread(ping_pong, "pong");
  }

}
