package com.company.epam.task1;

public class PingPongThread extends Thread {

  Ping_Pong ping_pong;
  String msg;

  public PingPongThread(Ping_Pong ping_pong, String msg) {
    this.ping_pong = ping_pong;
    this.msg = msg;
    this.start();
  }

  public String getMsg() {
    return msg;
  }

  @Override
  public void run(){
    int i = 0;
    while (i<10){
      i++;
      synchronized (ping_pong){
        System.out.println(PingPongThread.this.msg);
        ping_pong.notify();
        try {
          ping_pong.wait(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
