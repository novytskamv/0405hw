package com.company.epam.task4;

import com.company.epam.task2.Fibonacci;
import java.util.concurrent.Callable;

public class FibonacciCall implements Callable<Long> {

  Fibonacci fibonacci;
  Long sum = 0l;

  public FibonacciCall(Fibonacci fibonacci) {
    this.fibonacci = fibonacci;
  }

  @Override
  public Long call() throws Exception {
    for(Long l: fibonacci.produceF2()){
      sum += l;
    }
    System.out.println(Thread.currentThread().getName()+ " "+sum);
    return sum;
  }
}
