package com.company.epam.task4;

import com.company.epam.task2.Fibonacci;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class View {

  public static void main(String[] args) {

    FibonacciCall call1 = new FibonacciCall(new Fibonacci(50));
    FibonacciCall call2 = new FibonacciCall(new Fibonacci(50));

    ExecutorService ex = Executors.newFixedThreadPool(2);
    ex.submit(call1);
    ex.submit(call2);

  }

}
