package com.company.epam.task5;

import com.company.epam.task2.Fibonacci;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class View {

  public static void main(String[] args)
      throws ExecutionException, InterruptedException, TimeoutException {

    RandomGenerator gen = new RandomGenerator();
    Future<Long> fut1 = gen.generate();

    fut1.get();
    gen.es.shutdown();
  }
}
