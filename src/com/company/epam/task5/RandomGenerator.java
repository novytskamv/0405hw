package com.company.epam.task5;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RandomGenerator {

  ScheduledExecutorService es = Executors.newScheduledThreadPool(2);
  long ran;



  public Future<Long> generate() throws ExecutionException, InterruptedException {
    ran = ((long)(Math.random()*10+1))*1000;
    return es.schedule(()->
    {
      Thread.sleep(300);
      System.out.println(Thread.currentThread().getName()+" sleep is: "+ran);
      return ran;
    }, ran, TimeUnit.MILLISECONDS);
  }

}
