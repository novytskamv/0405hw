package com.company.epam.task6;

public class View {

  public static void main(String[] args) {

    MyClass mc1 = new MyClass();
    MyClass mc2 = new MyClass();
    MyClass mc3 = new MyClass();

    Thread thr1 = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          mc1.method1();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    });

    Thread thr2 = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          mc1.method2();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    });

    Thread thr3 = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          mc1.method3();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    });


//On diff objects
//    Thread thr1 = new Thread(new Runnable() {
//      @Override
//      public void run() {
//        try {
//          mc1.method1();
//        } catch (InterruptedException e) {
//          e.printStackTrace();
//        }
//      }
//    });
//
//    Thread thr2 = new Thread(new Runnable() {
//      @Override
//      public void run() {
//        try {
//          mc2.method2();
//        } catch (InterruptedException e) {
//          e.printStackTrace();
//        }
//      }
//    });
//
//    Thread thr3 = new Thread(new Runnable() {
//      @Override
//      public void run() {
//        try {
//          mc3.method3();
//        } catch (InterruptedException e) {
//          e.printStackTrace();
//        }
//      }
//    });

    thr1.start();
    thr2.start();
    thr3.start();


  }

}
