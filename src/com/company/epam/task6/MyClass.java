package com.company.epam.task6;

public class MyClass {

  int n;

  public void method1() throws InterruptedException {
    synchronized (this){
      for(int i = 0; i < 50; i++){
        ++n;
        Thread.sleep(100);
        System.out.println(Thread.currentThread().getName()+" method 1: "+n);
      }
    }
  }

  public void method2() throws InterruptedException {
    synchronized (this){
      for(int i = 0; i < 70; i++){
        ++n;
        Thread.sleep(100);
        System.out.println(Thread.currentThread().getName()+" method 2: "+n);
      }
    }
  }

  public void method3() throws InterruptedException {
    synchronized (this){
      for(int i = 0; i < 100; i++){
        ++n;
        Thread.sleep(100);
        System.out.println(Thread.currentThread().getName()+" method 3: "+n);
      }
  }
  }

}
