package com.company.epam.task2;

public class Fibonacci {

  int n;

  public Fibonacci(int n) {
    this.n = n;
  }

  public void produceF(){
    long fib [] = new long[n];
    fib[0] = 0;
    fib[1] = 1;
    for(int i = 2; i < fib.length; i++ ){
      fib[i]=fib[i-1]+fib[i-2];
    }
    for(long s: fib){
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println(FibonacciThread.currentThread().getName()+" "+s);
    }
  }

  public long[] produceF2(){
    long fib [] = new long[n];
    fib[0] = 0;
    fib[1] = 1;
    for(int i = 2; i < fib.length; i++ ){
      fib[i]=fib[i-1]+fib[i-2];
    }
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
    }
    return fib;
  }
}
