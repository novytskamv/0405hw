package com.company.epam.task2;

public class FibonacciThread extends Thread {

  Fibonacci fibonacci;
  String name;

  public FibonacciThread(Fibonacci fibonacci, String name) {
    this.fibonacci = fibonacci;
    this.name = name;
    this.start();
  }

  @Override
  public void run(){
        fibonacci.produceF();
  }
}
