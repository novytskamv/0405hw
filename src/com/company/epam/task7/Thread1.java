package com.company.epam.task7;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Thread1 implements Runnable {

public PipedOutputStream out = null;

  public Thread1(PipedOutputStream send) {
    this.out = send;
  }

  @Override
  public void run() {
    System.out.print("\nNumbers: ");
    for(int i = 0; i < 10; i++){
      int r = (int)(Math.random()*10+1);
      try {
        out.write(r);
      } catch (IOException e) {
        e.printStackTrace();
      }
      System.out.print(r+" ");
    }
    try {
      System.out.println();
      out.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
