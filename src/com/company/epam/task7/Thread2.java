package com.company.epam.task7;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PushbackInputStream;

public class Thread2 implements Runnable {

  PipedInputStream in = null;
  PipedOutputStream outNew = null;

  public Thread2(PipedInputStream get, PipedOutputStream send) {
    this.in = get;
    this.outNew = send;
  }

  @Override
  public void run() {
    try{
    int sum = 0;
    int  i = in.read();
    while (i != -1){
      if(i%2==0){
          sum += i;
      }
      else {
        try{
        outNew.write(i);
        } catch (IOException e){}
      }
      i = in.read();
    }
    outNew.close();
    System.out.println("\nSum of even numbers is: "+sum);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
