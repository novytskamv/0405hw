package com.company.epam.task7;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PushbackInputStream;

public class Thread3 implements Runnable {

PipedInputStream in = null;

  public Thread3(PipedInputStream get) {
    this.in = get;
  }

  @Override
  public void run() {
    try {
      int sum = 0;
      int i = in.read();
      while (i !=-1){
        sum += i;
        i = in.read();
      }
      in.close();
      System.out.println("\nSum of odds numbers is: "+sum);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
