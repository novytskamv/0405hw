package com.company.epam.task7;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class View {

  public static void main(String[] args) throws InterruptedException, IOException {
    final PipedOutputStream out = new PipedOutputStream();
    final PipedInputStream in = new PipedInputStream(out);
    final PipedOutputStream out2 = new PipedOutputStream();
    final PipedInputStream newIn = new PipedInputStream(out2);

    Thread1 th1 = new Thread1(out);
    Thread2 th2 = new Thread2(in, out2);
    Thread3 th3 = new Thread3(newIn);

    Thread t1 = new Thread(th1);
    Thread t2 = new Thread(th2);
    Thread t3 = new Thread(th3);

    t1.start();
    t2.start();
    t2.join();
    t3.start();
    t3.join();
    System.out.println("\nFinished");
  }

}
